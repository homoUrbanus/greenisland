Simple Crimsonland-like game.

Source code: /Source/
Executable file: /x64/Debug/PackageWin.exe

======================================

Game has a PAUSE option, which is available in "test mode" (see "-test" command-line argument).
If game is running in a test mode, pause is on at the starting of a game.
* to toggle pause press RIGHT MOUSE BUTTON.


===============CONTROLS===============

* Arrow keys - player moving
* Left mouse button - shot
* Right mouse button - pause game (test mode only!)

========COMMAND-LINE ARGUMENTS========

Some parameters can be defined via command-line arguments. If value is not defined, then the default value will be applied. Values can be defined in any order. For example:

PackageWin.exe -window 800x600 -map 500x500 -num_enemies 3 -num_ammo 2 -fullscreen true -test true

Command-line arguments instructions:
1) Test mode is configurable by command-line argument "-test" (true or false).
Default value: true
2) Game has a fullscreen option, which is configurable by command-line argument "-fullscreen" (true or false).
Default value: false
3) Window size can be configured by "-window" argument (example: 800x600).
Default value: 1400x1050
4) Map size can be configured by "-map" argument (example: 500x500). There are limited map sizes listed below; if argument has another value, then map will have the default size.
Default value: 1000x1000
Available map sizes:
* 500x500
* 1000x1000
* 1500x1500
5) Number of enemies can be configured by "-num_enemies" argument.
Default value: 10
6) Number of ammo can be configurable by "-num_ammo" argument.
Default value: 3