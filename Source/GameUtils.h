#pragma once
#include <Framework.h>

namespace GreenIsland
{
	struct Point
	{
		int x;
		int y;
	};

	struct Size
	{
		int width;
		int height;
	};

	enum Direction
	{
		NONE,
		RIGHT,
		LEFT,
		DOWN,
		UP
	};

	Direction getDirectionFromKeyPressed(FRKey k);

	bool isMapSizeAvailable(int mapWidth, int mapHeight);
}
