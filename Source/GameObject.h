#pragma once

#include "windows.h"

#include "Framework.h"
#include "GameUtils.h"
#include <vector>

namespace GreenIsland
{
	class GameObject
	{
	protected:
		char imgPath[MAX_PATH];
		Sprite* sprite;
		Point screenPos;
		Size spriteSize;

	public:
		GameObject(char* imagePath, Point initScreenPos);

		void init();
		void draw() const;
		const Size getSize();

		const Point getScreenPosition();
		void setScreenPosition(const Point newPos);
		void changeScreenPosition(const Point posDiff);

		~GameObject();
	};

	class MovableGameObject : public GameObject
	{
	protected:
		Point mapPos;
		Size mapSize;
		bool alive;
		
	public:
		MovableGameObject(char* imagePath, Point initScreenPos, Point initMapPos, Size mapSize);

		bool isAlive() const;
		void setAlive(bool value);
		Point getMapPosition() const;
		void setMapPosition(Point newPos);
		bool isInsideMap(const Point posDiff) const;
		bool changeMapPositionIfAble(const Point posDiff);
	};

	class BulletGameObject : public MovableGameObject
	{
	private:
		std::vector<Point> diagonalLine;

	public:
		BulletGameObject(char* imagePath, Point initScreenPos, Point initMapPos, Size mapSize)
		: MovableGameObject(imagePath, initScreenPos, initMapPos, mapSize) {}
		
		void evaluateDiagonalMovingLine(Point src, Point dest);
		Point nextMovingPoint();
	};
}
