#include "GameUtils.h"

GreenIsland::Direction GreenIsland::getDirectionFromKeyPressed(FRKey k)
{
	switch (k)
	{
	case FRKey::RIGHT:
		return Direction::RIGHT;
	case FRKey::LEFT:
		return Direction::LEFT;
	case FRKey::DOWN:
		return Direction::DOWN;
	case FRKey::UP:
		return Direction::UP;
	default:
		return Direction::NONE;
	}
}

bool GreenIsland::isMapSizeAvailable(int mapWidth, int mapHeight)
{
	return (mapWidth == 500 && mapHeight == 500)
		|| (mapWidth == 1000 && mapHeight == 1000)
		|| (mapWidth == 1500 && mapHeight == 1500);
}
