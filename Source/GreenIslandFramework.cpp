#include "GreenIslandFramework.h"
#include <iostream>
#include <deque>
#include <algorithm>
#include <vector>

void GreenIsland::GreenIslandFramework::evaluateDirectionalPoint(Point& directionalPoint)
{
	if (assignedDirections[Direction::RIGHT])
	{
		directionalPoint.x = 1;
	}
	if (assignedDirections[Direction::LEFT])
	{
		directionalPoint.x = -1;
	}
	if ((!assignedDirections[Direction::RIGHT] && !assignedDirections[Direction::LEFT])
		|| (assignedDirections[Direction::RIGHT] && assignedDirections[Direction::LEFT]))
	{
		directionalPoint.x = 0;
	}
	if (assignedDirections[Direction::DOWN])
	{
		directionalPoint.y += 1;
	}
	if (assignedDirections[Direction::UP])
	{
		directionalPoint.y += -1;
	}
	if ((!assignedDirections[Direction::DOWN] && !assignedDirections[Direction::UP])
		|| (assignedDirections[Direction::DOWN] && assignedDirections[Direction::UP]))
	{
		directionalPoint.y = 0;
	}
}

void GreenIsland::GreenIslandFramework::spawnAndInitEnemies(int quantity, char* imgPath)
{
	while (quantity > 0)
	{
		enemies.push_back(spawnObjectOnRandomPoint(imgPath));
		quantity--;
	}
}

void GreenIsland::GreenIslandFramework::spawnAndInitBullets(int quantity, char* imgPath)
{
	while (quantity > 0)
	{
		BulletGameObject* bullet = new BulletGameObject(imgPath, { 0,0 }, { 0,0 }, mapSize);
		bullet->init();
		bullet->setAlive(false);
		bullets.push_back(bullet);
		quantity--;
	}
}

void GreenIsland::GreenIslandFramework::setRandomPositionToObject(MovableGameObject*& obj)
{
	Size spriteSize = obj->getSize();
	bool isPointInvalid = true;
	Point mapPos;
	while (isPointInvalid)
	{
		mapPos = { rand() % (mapSize.width - spriteSize.width),
			rand() % (mapSize.height - spriteSize.height)};
		obj->setMapPosition(mapPos);
		isPointInvalid = hasCollisionsWhenSpawn(obj);
	}
	obj->changeScreenPosition({ -obj->getScreenPosition().x, -obj->getScreenPosition().y });
	obj->changeScreenPosition({ map->getScreenPosition().x + mapPos.x, map->getScreenPosition().y + mapPos.y });
}

GreenIsland::MovableGameObject* GreenIsland::GreenIslandFramework::spawnObjectOnRandomPoint(char* imgPath)
{
	MovableGameObject* obj = new MovableGameObject(imgPath, { 0,0 }, { 0,0 }, mapSize);
	obj->init();
	setRandomPositionToObject(obj);
	return obj;
}

bool GreenIsland::GreenIslandFramework::hasCollisionsWhenSpawn(MovableGameObject*& movObj)
{
	if (hasCollision(movObj->getMapPosition(), movObj->getSize(),
		player->getMapPosition(), player->getSize()))
	{
		return true;
	}
	MovableGameObject* collidedObjPtr = nullptr;
	bool hasEnemyCollision = findCollisionEnemy(movObj, collidedObjPtr);
	return hasEnemyCollision;
}

bool GreenIsland::GreenIslandFramework::hasCollision(Point movPoint, Size movSize, Point targetPoint, Size targetSize)
{
	return !(((movPoint.x + movSize.width) < (targetPoint.x) || (movPoint.x > (targetPoint.x + targetSize.width)))
		|| ((movPoint.y + movSize.height) < (targetPoint.y) || (movPoint.y > (targetPoint.y + targetSize.height))));
}

bool GreenIsland::GreenIslandFramework::hasCollisionEnemy(MovableGameObject*& movObj)
{
	MovableGameObject* dummy;
	return findCollisionEnemy(movObj, dummy);
}

bool GreenIsland::GreenIslandFramework::findCollisionEnemy(MovableGameObject*& movObj, MovableGameObject*& emptyCollidedObj)
{
	for (MovableGameObject* enemy : enemies)
	{
		if (movObj != enemy && enemy->isAlive() && hasCollision(movObj->getMapPosition(), movObj->getSize(), 
			enemy->getMapPosition(), enemy->getSize()))
		{
			emptyCollidedObj = enemy;
			return true;
		}
	}
	return false;
}


GreenIsland::Point GreenIsland::GreenIslandFramework::getPixelMoveToDirection(Point src, Point dest)
{
	Point result{ 0,0 };
	const int deltaX = abs(dest.x - src.x);
	const int deltaY = abs(dest.y - src.y);
	const int signX = src.x < dest.x ? 1 : -1;
	const int signY = src.y < dest.y ? 1 : -1;

	const int errorX2 = (deltaX - deltaY) * 2;
	Point tempPoint = { src.x + signX, src.y + signY };
	if (errorX2 > -deltaY && tempPoint.x >= 0 && tempPoint.x <= mapSize.width)
	{
		result.x = signX;
	}
	if (errorX2 < deltaX && tempPoint.y >= 0 && tempPoint.y <= mapSize.height)
	{
		result.y = signY;
	}
	return result;
}

GreenIsland::Point GreenIsland::GreenIslandFramework::getAroundOfObstacleIfAble(MovableGameObject*& movObj, MovableGameObject*& colObj, Point oldDir)
{
	const Point movPos = movObj->getMapPosition();
	const Size movSize = movObj->getSize();
	const Point colPos = colObj->getMapPosition();
	const Size colSize = colObj->getSize();
	Point res = { oldDir.x, oldDir.y };
	// if collided by x axis ==> getting around by y axis; same for y-axis collision
	if (movPos.x == colPos.x + colSize.width || movPos.x + movSize.width == colPos.x)
	{
		res.x += -oldDir.x;
		res.y = movPos.y - colPos.y < (colPos.y + colSize.height) - movPos.y
			? - 1
			: 1;
	}
	if (movPos.y == colPos.y + colSize.height || movPos.y + movSize.height == colPos.y)
	{
		res.y = -oldDir.y;
		res.x = movPos.x - colPos.x < (colPos.x + colSize.width) - movPos.x
			? - 1
			: 1;
	}
	return res;
}

GreenIsland::Point GreenIsland::GreenIslandFramework::lengthenDestinationToNearestMapBorder(Point src, Point dest)
{
	const Size mapSize = map->getSize();
	if (dest.x == 0 || dest.x == mapSize.width || dest.y == 0 || dest.y == mapSize.height)
	{
		return dest;
	}

	Point vector = { dest.x - src.x, dest.y - src.y };
	int xBorder = vector.x <= 0
		? 0
		: mapSize.width;
	int yBorder = vector.y <= 0
		? 0
		: mapSize.height;
	if (vector.x == 0)
	{
		return lengthenByYAxis(src, dest, yBorder);
	}
	if (vector.y == 0)
	{
		return lengthenByXAxis(src, dest, 0);
	}
	
	Point lengthenRes = lengthenByXAxis(src, dest, xBorder);
	if (lengthenRes.y >= 0 && lengthenRes.y <= mapSize.height)
	{
		return lengthenRes;
	}

	lengthenRes = lengthenByYAxis(src, dest, yBorder);
	if (lengthenRes.x >= 0 && lengthenRes.x <= mapSize.width)
	{
		return lengthenRes;
	}
	
	return { mapSize.width, mapSize.height };
}

GreenIsland::Point GreenIsland::GreenIslandFramework::lengthenByXAxis(Point src, Point dest, int xBorder)
{
	double a;
	double b;
	findEquationCoefficients(src, dest, a, b);
	const int y = a * xBorder + b;
	return { xBorder, y };
}

GreenIsland::Point GreenIsland::GreenIslandFramework::lengthenByYAxis(Point src, Point dest, int yBorder)
{
	double a;
	double b;
	findEquationCoefficients(src, dest, a, b);
	const int x = (yBorder - b) / a;
	return { x, yBorder };
}

void GreenIsland::GreenIslandFramework::findEquationCoefficients(Point src, Point dest, double& a, double& b)
{
	double d = src.x - dest.x;
	a = (src.y - dest.y) / d;
	b = (src.x * dest.y - dest.x * src.y) / d;
}

void GreenIsland::GreenIslandFramework::shotBulletAt(int bulletIdx)
{
	Point cursorMapPos = { cursor->getScreenPosition().x - map->getScreenPosition().x,
					cursor->getScreenPosition().y - map->getScreenPosition().y };
	Size playerSize = player->getSize();
	Size bulletSize = bullets[bulletIdx]->getSize();
	Point playerMapCenter = { player->getMapPosition().x + (playerSize.width / 2) - bulletSize.width,
		player->getMapPosition().y + (playerSize.height / 2) - bulletSize.height };
	Point playerScreenCenter = { player->getScreenPosition().x + (playerSize.width / 2) - bulletSize.width,
		player->getScreenPosition().y + (playerSize.height / 2) - bulletSize.height };

	bullets[bulletIdx]->setMapPosition(playerMapCenter);
	bullets[bulletIdx]->setScreenPosition(playerScreenCenter);
	bullets[bulletIdx]->evaluateDiagonalMovingLine(playerMapCenter, 
		lengthenDestinationToNearestMapBorder(playerMapCenter, cursorMapPos));
	bullets[bulletIdx]->setAlive(true);
}

void GreenIsland::GreenIslandFramework::killBulletAt(int bulletIdx)
{
	bullets[bulletIdx]->setAlive(false);
	BulletGameObject* bullet = bullets[bulletIdx];
	bullets.erase(bullets.begin() + bulletIdx);
	bullets.push_back(bullet);
}



void GreenIsland::GreenIslandFramework::initMapObject()
{
	char mapImgFullPath[MAX_PATH];
	sprintf_s(mapImgFullPath, "%s%s%d%s%d%s", currentDir, MAP_IMG_RELATIVE_PATH, mapSize.width,
		MAP_IMG_SIZE_DELIMITER, mapSize.height, MAP_IMG_FORMAT);
	map = new GameObject(mapImgFullPath, { windowSize.width / 2, windowSize.height / 2 });

	map->init();

	map->changeScreenPosition({ -map->getSize().width / 2, -map->getSize().height / 2 });
}

void GreenIsland::GreenIslandFramework::initPlayerObject()
{
	char playerImgFullPath[MAX_PATH];
	sprintf_s(playerImgFullPath, "%s%s", currentDir, PLAYER_IMG_RELATIVE_PATH);
	player = new MovableGameObject(playerImgFullPath, { windowSize.width / 2, windowSize.height / 2 },
		{ mapSize.width / 2, mapSize.height / 2 }, mapSize);
	player->init();

	Size playerSize = player->getSize();

	player->changeScreenPosition({ -playerSize.width / 2, -playerSize.height / 2 });
	player->changeMapPositionIfAble({ -playerSize.width / 2, -playerSize.height / 2 });

	playerSizeWithSpace = { (int)(playerSize.width * PLAYER_SPACE_THRESHOLD_COEFFICIENT) + playerSize.width,
		(int)(playerSize.height * PLAYER_SPACE_THRESHOLD_COEFFICIENT) + playerSize.height };
}

void GreenIsland::GreenIslandFramework::initEnemiesObject()
{
	char enemiesImgFullPath[MAX_PATH];
	sprintf_s(enemiesImgFullPath, "%s%s", currentDir, ENEMY_IMG_RELATIVE_PATH);
	spawnAndInitEnemies(numEnemies, enemiesImgFullPath);
}

void GreenIsland::GreenIslandFramework::initBullets()
{
	char bulletsImgFullPath[MAX_PATH];
	sprintf_s(bulletsImgFullPath, "%s%s", currentDir, BULLET_IMG_RELATIVE_PATH);
	spawnAndInitBullets(numAmmo, bulletsImgFullPath);
}

void GreenIsland::GreenIslandFramework::initCursor()
{
	char cursorImgFullPath[MAX_PATH];
	sprintf_s(cursorImgFullPath, "%s%s", currentDir, CURSOR_IMG_RELATIVE_PATH);
	cursor = new GameObject(cursorImgFullPath, { windowSize.width / 2, windowSize.height / 2 });

	cursor->init();
}

void GreenIsland::GreenIslandFramework::restartGame()
{
	player->setMapPosition({ (mapSize.width / 2) - (player->getSize().width / 2), 
		(mapSize.height / 2) - (player->getSize().height / 2) });
	player->setScreenPosition({ (windowSize.width / 2) - (player->getSize().width / 2), 
		(windowSize.height / 2) - (player->getSize().height / 2) });

	map->setScreenPosition({ (windowSize.width / 2) - (mapSize.width / 2),
		(windowSize.height / 2) - (mapSize.height / 2) });

	for (MovableGameObject* enemy : enemies)
	{
		setRandomPositionToObject(enemy);
		enemy->setAlive(true);
	}

	for (MovableGameObject* bullet: bullets)
	{
		bullet->setAlive(false);
	}
}




GreenIsland::GreenIslandFramework::GreenIslandFramework(char* curDir, const Size windowSize, bool fullScreen, bool testMode,
	const Size mapSize, int numEnemies, int numAmmo)
	: windowSize(windowSize),
	fullScreen(fullScreen),
	testMode(testMode),
	mapSize(mapSize),
	numEnemies(numEnemies),
	numAmmo(numAmmo),
	currentDir(curDir)
{
	playerMoveDir = { 0, 0 };
	pause = testMode;
}

void GreenIsland::GreenIslandFramework::PreInit(int& width, int& height, bool& fullscreen)
{
	width = windowSize.width;
	height = windowSize.height;
	fullscreen = fullScreen;
}

bool GreenIsland::GreenIslandFramework::Init()
{
	initMapObject();
	initPlayerObject();
	initEnemiesObject();
	initBullets();
	initCursor();
	
	return true;
}

void GreenIsland::GreenIslandFramework::Close()
{

}

bool GreenIsland::GreenIslandFramework::Tick()
{
	drawTestBackground();

	if (!pause)
	{
		if (player->changeMapPositionIfAble(playerMoveDir))
		{
			map->changeScreenPosition({ -playerMoveDir.x, -playerMoveDir.y });
			for (MovableGameObject* enemy : enemies)
			{
				if (enemy->isAlive())
				{
					enemy->changeScreenPosition({ -playerMoveDir.x, -playerMoveDir.y });
				}
			}
			for (MovableGameObject* bullet : bullets)
			{
				if (bullet->isAlive())
				{
					bullet->changeScreenPosition({ -playerMoveDir.x, -playerMoveDir.y });
				}
			}
		}

		bool hasAliveEnemies = false;
		
		for (MovableGameObject* enemy : enemies)
		{
			if (enemy->isAlive())
			{
				if (!hasAliveEnemies)
				{
					hasAliveEnemies = true;
				}
				
				Point newDir = getPixelMoveToDirection(enemy->getMapPosition(), player->getMapPosition());
				if (enemy->changeMapPositionIfAble(newDir))
				{
					enemy->changeScreenPosition(newDir);
				}
				
				MovableGameObject* collidedObj;
				if (findCollisionEnemy(enemy, collidedObj))
				{
					newDir = getAroundOfObstacleIfAble(enemy, collidedObj, newDir);
					if (enemy->changeMapPositionIfAble(newDir))
					{
						enemy->changeScreenPosition(newDir);
					}
				}
			}
		}
		BulletGameObject* bullet;
		for (int i = 0; i < bullets.size(); ++i)
		{
			bullet = bullets[i];
			if (bullet->isAlive())
			{
				Point curMapPos = bullet->getMapPosition();
				Point newMapPos = bullet->nextMovingPoint();
				Point newDir = { newMapPos.x - curMapPos.x, newMapPos.y - curMapPos.y };
				if (bullet->changeMapPositionIfAble(newDir))
				{
					bullet->changeScreenPosition(newDir);
				}
				Size bulletSize = bullet->getSize();
				bool collidedByXAxis = newMapPos.x <= 0 || newMapPos.x >= mapSize.width - bulletSize.width;
				bool collidedByYAxis = newMapPos.y <= 0 || newMapPos.y >= mapSize.height - bulletSize.height;
				
				if (collidedByXAxis || collidedByYAxis)
				{
					killBulletAt(i);
					continue;
				}
				
				MovableGameObject* collidedEnemy;
				if (findCollisionEnemy((MovableGameObject*&) bullet, collidedEnemy))
				{
					killBulletAt(i);
					collidedEnemy->setAlive(false);
				}
			}
		}
		
		if (hasCollisionEnemy(player) || !hasAliveEnemies)
		{
			restartGame();
		}
	}
	
	map->draw();
	
	player->draw();

	for (MovableGameObject* enemy : enemies)
	{
		if (enemy->isAlive())
		{
			enemy->draw();
		}
	}

	for (MovableGameObject* bullet : bullets)
	{
		if (bullet->isAlive())
		{
			bullet->draw();
		}
	}
	
	cursor->draw();
	showCursor(false);
	
	return false;
}

void GreenIsland::GreenIslandFramework::onMouseMove(int x, int y, int xrelative, int yrelative)
{
	cursor->setScreenPosition({ x, y });
}

void GreenIsland::GreenIslandFramework::onMouseButtonClick(FRMouseButton button, bool isReleased)
{
	if (testMode && FRMouseButton::RIGHT == button && !isReleased)
	{
		pause = !pause;
	}
	if (!pause && FRMouseButton::LEFT == button && !isReleased)
	{
		BulletGameObject* bullet;
		for (int i = 0; i < bullets.size(); ++i)
		{
			bullet = bullets[i];
			if (!bullet->isAlive())
			{
				shotBulletAt(i);
				return;
			}
		}
		shotBulletAt(0);
		bullet = bullets[0];
		for (int i = 0; i < bullets.size() - 1; ++i)
		{
			bullets[i] = bullets[i + 1];
		}
		bullets[bullets.size() - 1] = bullet;
	}
}

void GreenIsland::GreenIslandFramework::onKeyPressed(FRKey k)
{
	Direction newDir = getDirectionFromKeyPressed(k);
	if (Direction::NONE != newDir)
	{
		assignedDirections[newDir] = true;
	}
	evaluateDirectionalPoint(playerMoveDir);
}

void GreenIsland::GreenIslandFramework::onKeyReleased(FRKey k)
{
	Direction newDir = getDirectionFromKeyPressed(k);
	if (Direction::NONE != newDir)
	{
		assignedDirections[newDir] = false;
	}
	evaluateDirectionalPoint(playerMoveDir);
}

GreenIsland::GreenIslandFramework::~GreenIslandFramework()
{
	delete map;
	delete player;
	delete cursor;
}