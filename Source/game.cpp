#include <vector>
#include "windows.h"

#include "GameUtils.h"

#include "Framework.h"
#include "GreenIslandFramework.h"

void getCommandLineArgumentsIfAble(int argc, char* argv[], GreenIsland::Size& windowSize, bool& fullScreen,
    bool& testMode, GreenIsland::Size& mapSize, int& numEnemies, int& numAmmo)
{
	for (int argIdx = 1; argIdx < argc - 1; argIdx++)
	{
		char* argumentName = argv[argIdx];
		char* argumentValue;
		if (strcmp(argumentName, "-window") == 0)
		{
			++argIdx;
			argumentValue = argv[argIdx];
			sscanf_s(argumentValue, "%dx%d", &windowSize.width, &windowSize.height);
		}
		else if (strcmp(argumentName, "-map") == 0)
		{
			++argIdx;
			argumentValue = argv[argIdx];
			int mapWidth = 0;
			int mapHeight = 0;
			sscanf_s(argumentValue, "%dx%d", &mapWidth, &mapHeight);
			if (GreenIsland::isMapSizeAvailable(mapWidth, mapHeight))
			{
				mapSize.width = mapWidth;
				mapSize.height = mapHeight;
			}
		}
		else if (strcmp(argumentName, "-num_enemies") == 0)
		{
			++argIdx;
			argumentValue = argv[argIdx];
			sscanf_s(argumentValue, "%d", &numEnemies);
		}
		else if (strcmp(argumentName, "-num_ammo") == 0)
		{
			++argIdx;
			argumentValue = argv[argIdx];
			sscanf_s(argumentValue, "%d", &numAmmo);
		}
		else if (strcmp(argumentName, "-fullscreen") == 0)
		{
			++argIdx;
			argumentValue = argv[argIdx];
			fullScreen = strcmp(argumentValue, "true") == 0;
		}
		else if (strcmp(argumentName, "-test") == 0)
		{
			++argIdx;
			argumentValue = argv[argIdx];
			testMode = strcmp(argumentValue, "true") == 0;
		}
	}
}

int main(int argc, char* argv[])
{
	char curDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, curDir);
	
	GreenIsland::Size windowSize = { 1400, 1050 };
	GreenIsland::Size mapSize = { 1000, 1000 };
	bool fullScreen = false;
	bool testMode = false;
	int numEnemies = 10;
	int numAmmo = 3;

	getCommandLineArgumentsIfAble(argc, argv, windowSize, fullScreen, testMode, mapSize, numEnemies, numAmmo);
	
	GreenIsland::GreenIslandFramework* greenIslandFramework =
		new GreenIsland::GreenIslandFramework(curDir, windowSize,
			fullScreen, testMode, mapSize, numEnemies, numAmmo);
	return run(greenIslandFramework);
}
