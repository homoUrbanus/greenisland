#pragma once

#include <vector>

#include <Framework.h>

#include "GameUtils.h"
#include "GameObject.h"
#include <map>


namespace GreenIsland
{
	/* Test Framework realization */
	class GreenIslandFramework : public Framework {

	private:
		const float PLAYER_SPACE_THRESHOLD_COEFFICIENT = 1.5f;
		
		const char* PLAYER_IMG_RELATIVE_PATH = "\\data\\avatar.jpg";
		const char* MAP_IMG_RELATIVE_PATH = "\\data\\map";
		const char* MAP_IMG_SIZE_DELIMITER = "x";
		const char* MAP_IMG_FORMAT = ".png";
		const char* ENEMY_IMG_RELATIVE_PATH = "\\data\\enemy.png";
		const char* BULLET_IMG_RELATIVE_PATH = "\\data\\bullet.png";
		const char* CURSOR_IMG_RELATIVE_PATH = "\\data\\circle.tga";

		std::map<Direction, bool> assignedDirections =
		{
			{ Direction::RIGHT, false },
			{ Direction::LEFT, false },
			{ Direction::DOWN, false },
			{ Direction::UP, false }
		};

		Size windowSize;
		bool fullScreen;
		bool testMode;

		Size mapSize;

		int numEnemies;
		int numAmmo;

		char* currentDir;

		bool pause;
		
		GameObject* map;
		MovableGameObject* player;
		std::vector<MovableGameObject*> enemies;
		std::vector<BulletGameObject*> bullets;

		GameObject* cursor;

		Point playerMoveDir;
		Size playerSizeWithSpace;

		void evaluateDirectionalPoint(Point& directionalPoint);
		void spawnAndInitEnemies(int quantity, char* imgPath);
		void spawnAndInitBullets(int quantity, char* imgPath);
		void setRandomPositionToObject(MovableGameObject*& obj);
		MovableGameObject* spawnObjectOnRandomPoint(char* imgPath);

		bool hasCollisionsWhenSpawn(MovableGameObject*& movObj);
		bool hasCollision(Point movPoint, Size movSize, Point targetPoint, Size targetSize);

		bool hasCollisionEnemy(MovableGameObject*& movObj);
		bool findCollisionEnemy(MovableGameObject*& movObj, MovableGameObject*& emptyCollidedObj);
		
		Point getPixelMoveToDirection(Point src, Point dest);
		Point getAroundOfObstacleIfAble(MovableGameObject*& movObj, MovableGameObject*& colObj, Point oldDir);

		Point lengthenDestinationToNearestMapBorder(Point src, Point dest);
		Point lengthenByXAxis(Point src, Point dest, int xBorder);
		Point lengthenByYAxis(Point src, Point dest, int yBorder);
		void findEquationCoefficients(Point src, Point dest, double& a, double& b);

		void shotBulletAt(int bulletIdx);
		void killBulletAt(int bulletIdx);
		
		void initMapObject();
		void initPlayerObject();
		void initEnemiesObject();
		void initBullets();
		void initCursor();

		void restartGame();
		
	public:

		GreenIslandFramework(char* curDir, const Size windowSize, bool fullScreen, bool testMode, 
			const Size mapSize, int numEnemies, int numAmmo);

		virtual void PreInit(int& width, int& height, bool& fullscreen);
		virtual bool Init();

		virtual void Close();
		virtual bool Tick();

		virtual void onMouseMove(int x, int y, int xrelative, int yrelative);
		virtual void onMouseButtonClick(FRMouseButton button, bool isReleased);
		virtual void onKeyPressed(FRKey k);
		virtual void onKeyReleased(FRKey k);

		~GreenIslandFramework();
	};
}
