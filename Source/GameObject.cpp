#include "GameObject.h"

#include "windows.h"


// GameObject

GreenIsland::GameObject::GameObject(char* imagePath, Point initialPosition)
	: screenPos(initialPosition)
{
	strcpy_s(imgPath, imagePath);
}

void GreenIsland::GameObject::init()
{
	sprite = createSprite(imgPath);
	getSpriteSize(sprite, spriteSize.width, spriteSize.height);
}

void GreenIsland::GameObject::draw() const
{
	drawSprite(sprite, screenPos.x, screenPos.y);
}

const GreenIsland::Size GreenIsland::GameObject::getSize()
{
	return spriteSize;
}

const GreenIsland::Point GreenIsland::GameObject::getScreenPosition()
{
	return screenPos;
}


void GreenIsland::GameObject::setScreenPosition(const Point newPos)
{
	screenPos.x = newPos.x;
	screenPos.y = newPos.y;
}

void GreenIsland::GameObject::changeScreenPosition(const Point posDiff)
{
	screenPos.x += posDiff.x;
	screenPos.y += posDiff.y;
}


GreenIsland::GameObject::~GameObject()
{
	destroySprite(sprite);
}


// MovableGameObject


GreenIsland::MovableGameObject::MovableGameObject(char* imagePath, Point initScreenPos, Point initMapPos, Size mapSize) : GameObject(imagePath, initScreenPos)
{
	this->mapSize = mapSize;
	mapPos = initMapPos;
	alive = true;
}

bool GreenIsland::MovableGameObject::isAlive() const
{
	return alive;
}

void GreenIsland::MovableGameObject::setAlive(bool value)
{
	alive = value;
}


GreenIsland::Point GreenIsland::MovableGameObject::getMapPosition() const
{
	return mapPos;
}

void GreenIsland::MovableGameObject::setMapPosition(Point newPos)
{
	mapPos = newPos;
}


bool GreenIsland::MovableGameObject::isInsideMap(const Point posDiff) const
{
	Point newPos = { mapPos.x + posDiff.x , mapPos.y + posDiff.y };
	return newPos.x >= 0 && newPos.x <= mapSize.width - spriteSize.width
		&& newPos.y >= 0 && newPos.y <= mapSize.height - spriteSize.height;
}

bool GreenIsland::MovableGameObject::changeMapPositionIfAble(const Point posDiff)
{
	if (isInsideMap(posDiff))
	{
		mapPos.x += posDiff.x;
		mapPos.y += posDiff.y;
		return true;
	}
	return false;
}



// BulletGameObject


void GreenIsland::BulletGameObject::evaluateDiagonalMovingLine(Point src, Point dest)
{
	if (!diagonalLine.empty())
	{
		diagonalLine.clear();
	}
	
	int deltaX = abs(dest.x - src.x);
	int deltaY = abs(dest.y - src.y);
	int error = 0;
	int deltaError = (deltaY + 1);
	int y = src.y;
	int dirY = (dest.y - src.y > 0) - (dest.y - src.y < 0);
	int dirX = (dest.x - src.x > 0) - (dest.x - src.x < 0);
	
	for (int x = src.x; x != dest.x + dirX && y != dest.y + dirY; x += dirX)
	{
		diagonalLine.push_back({ x,y });
		error += deltaError;
		if (error >= deltaX + 1)
		{
			y += dirY;
			error -= deltaX + 1;
		}
	}
}

GreenIsland::Point GreenIsland::BulletGameObject::nextMovingPoint()
{
	if (diagonalLine.empty())
	{
		return { 0,0 };
	}
	Point res = diagonalLine.front();
	diagonalLine.erase(diagonalLine.begin());
	return res;
}
